CONTENTS OF THIS FILE
---------------------

* Introduction
* Recommended modules
* Installation
* Configuration
* Documentation
* Requirements


INTRODUCTION
------------

A Community is a set of people with a particular interest in common. Unlike groups, which are usually reserved for people that know each other, Communities often welcome people from around the world and can have up to 1 billion members.

Communities also have advanced moderation features, including admins being able to ban members, remove content sent by other people, and only permit messages sent by other admins.

For more information on the type of content permitted in Communities, please review Viber’s Acceptable Use Policy. Communities that don’t adhere to the guidelines may be closed.


RECOMMENDED MODULES
-------------------

* No extra module is required.


INSTALLATION
------------

* [Install as usual, see](https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8) for further information.


CONFIGURATION
-------------

* Navigate to Administration > Configuration > Web services > Viber Channel
* [How to create a Community](https://help.viber.com/en/article/communities-knowledge-base-for-admins)

DOCUMENTATION
-------------

* You can find out more — [Viber API Documentation](https://developers.viber.com/docs/tools/channels-post-api)

REQUIREMENTS
------------

* No extra module is required.

