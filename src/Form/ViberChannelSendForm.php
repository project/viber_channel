<?php

namespace Drupal\viber_channel\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\viber_channel\API\ViberApi;
use Drupal\viber_channel\Http\HttpClient;
use Drupal\viber_channel\Model\ViberChannelServiceInterface;

/**
 * Form for send message to viber channel.
 */
class ViberChannelSendForm extends FormBase {

  /**
   * Folder for save temporality photo which will be sent.
   */
  const FOLDER = 'public://viber/channel_files/';

  /**
   * Type of message.
   */
  const DEF_TYPE = 'text';

  /**
   * Available tags for send to viber.
   */
  const TAGS = '';

  /**
   * @var ViberChannelServiceInterface|null
   */
  protected $viberService = NULL;

  /**
   * @var \Drupal\viber_channel\Query\ViberChannelQuery|null
   */
  private $viberQuery = NULL;

  /**
   * Provides an interface for entity type managers.
   *
   * @var EntityTypeManagerInterface|NULL
   */
  private $entityTypeManager;

  /**
   * Provides an interface for an entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface|null
   */
  private $entityRepository = NULL;

  /**
   * Request represents an HTTP request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  private $request = NULL;

  /**
   * General interface for coordinating the receipt of content from a remote
   * api.
   *
   * @var \Drupal\viber_channel\API\IContentRequestManager|null
   */
  private $viberRequest = NULL;

  /**
   * @var \Drupal\Core\Render\Renderer|null
   */
  private $renderer = NULL;

  /**
   * @var \Drupal\Core\File\FileUrlGenerator|null
   */
  private $fileUrlGenerator = NULL;

  /**
   * @var \Drupal\Core\Extension\ModuleHandler|NULL
   */
  private $moduleHandler = NULL;

  /**
   * Construct of ViberChannelSendForm object.
   */
  public function __construct() {
    $this->initProperties();
  }

  /**
   * Initialization of all properties.
   */
  public function initProperties() {
    $container = \Drupal::getContainer();
    $this->viberService = $container->get('viber_channel');
    $this->viberQuery = $container->get('viber_channel.query');
    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->entityRepository = $container->get('entity.repository');
    $this->request = $container->get('request_stack')->getCurrentRequest();
    $this->viberRequest = $container->get('viber_channel.request');
    $this->renderer = $container->get('renderer');
    $this->fileUrlGenerator = $container->get('file_url_generator');
    $this->moduleHandler = $container->get('module_handler');
  }

  /**
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function deleteFiles() {
    $fids = \Drupal::database()
      ->select('file_managed', 'f')
      ->fields('f', ['fid'])
      ->condition('f.uri', self::FOLDER . '%', 'LIKE')
      ->execute()
      ->fetchCol();
    if (!empty($fids)) {
      foreach (File::loadMultiple($fids) as $file) {
        $file->delete();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form_id = $this->getFormId();

    if (!$this->viberQuery) {
      $this->initProperties();
    }

    $listOfChannelUsers = $this->viberQuery->getUsers();
    if (!$listOfChannelUsers) {
      return ['#markup' => $this->t('Users not found')];
    }

    $storage = $form_state->getStorage();
    if (empty($form_state->getValues())) {
      $form_state->setValue('type', $this::DEF_TYPE);
    }
    if (empty($form_state->getValue('type')) && !empty($storage['type'])) {
      $form_state->setValue('type', $storage['type']);
    }
    $storage['type'] = $form_state->getValue('type');
    $form_state->setStorage($storage);

    $form['#prefix'] = "<div id='$form_id'>";
    $form['#suffix'] = '</div>';

    $tokens = array_column($listOfChannelUsers, 'sid');
    $names = array_column($listOfChannelUsers, 'user_name');
    $options = array_combine($tokens, $names);

    $form['sender'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose sender'),
      '#description' => $this->t('On whose behalf the messages will be sent'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Message type'),
      '#required' => TRUE,
      '#options' => [
        'text' => $this->t('Message'),
        'photo' => $this->t('Photo with caption'),
      ],
      '#default_value' => $form_state->getValue('type'),
      '#ajax' => [
        'wrapper' => $form_id,
        'callback' => [$this, 'ajaxCallback'],
        'event' => 'change',
      ],
    ];

    $messageInfo = $this->getMessageValue($form_state->getValue('type'), $form_state);
    switch ($form_state->getValue('type')) {
      case'text':
        $form['message'] = [
          '#type' => 'text_format',
          '#title' => $this->t('Message'),
          '#format' => 'viber_html',
          '#after_build' => [[$this, 'textFormatHide']],
          '#default_value' => !empty($messageInfo['message']) ? $messageInfo['message'] : '',
          '#required' => TRUE,
          '#description' => $this->t('The signature should not contain links and be more than 7000 characters.'),
        ];
        break;
      case'photo':
        $form['file'] = [
          '#type' => 'managed_file',
          '#title' => t('File'),
          '#upload_location' => $this::FOLDER,
          '#required' => TRUE,
          '#upload_validators' => [
            'file_validate_extensions' => ['png gif jpg jpeg'],
          ],
          '#default_value' => !empty($messageInfo['photo']['fid']) ? [$messageInfo['photo']['fid']] : NULL,
        ];
        $form['caption'] = [
          '#type' => 'text_format',
          '#title' => $this->t('Caption'),
          '#format' => 'viber_html',
          '#after_build' => [[$this, 'textFormatHide']],
          '#default_value' => !empty($messageInfo['photo']['caption']) ? $messageInfo['photo']['caption'] : '',
          '#description' => $this->t('The signature should not contain links and be more than 768 characters.'),
        ];
        break;
    }

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 100,
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send'),
        '#name' => 'send',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'viber_channel_send_form';
  }

  /**
   * @param $type
   * @param $form_state
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getMessageValue($type, $form_state): array {
    $items = [];
    if (!empty($nid = $this->request->query->get('send_nid'))) {
      $entity = $this->entityTypeManager->getStorage('node')->load($nid);
      if (!empty($entity)) {
        /** @var \Drupal\node\Entity\Node $entity */
        $entity = $this->entityRepository->getTranslationFromContext($entity);
        $items[] = $entity->getTitle();
        $items[] = Url::fromRoute('entity.node.canonical', ['node' => $entity->id()], ['absolute' => TRUE])
          ->toString();
      }
      $form_state->set('sendNid', $nid);
    }
    $message = implode(PHP_EOL, $items);
    $messageInfo = [
      'message' => $message,
      'photo' => [
        'fid' => 0,
        'caption' => $message,
      ],
    ];

    /** Create alter for message */
    $this->moduleHandler->alter('viber_channel_message', $messageInfo, $type);

    return $messageInfo;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function ajaxCallback(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->viberQuery) {
      $this->initProperties();
    }
    if (!empty($form_state->get('sendRun'))) {
      return;
    }

    $sender = $form_state->getValue('sender');
    $token = \Drupal::config(ViberChannelSettingsForm::CONFIG_NAME)
      ->get('config')['auth_token'];

    $triggerElement = $form_state->getTriggeringElement();
    $messageType = $form_state->getValue('type');

    $client = (new HttpClient())->setEndpointUri(ViberApi::POST)
      ->withBody('auth_token', $token)
      ->withBody('from', $sender)
      ->setMethod('POST');

    if (!empty($triggerElement['#name']) && $triggerElement['#name'] == 'send') {
      $markup = '';

      /**
       * Max 7000 characters for text and 768 for picture.
       *
       * @see https://developers.viber.com/docs/tools/channels-post-api/#message-types
       * @see https://developers.viber.com/docs/tools/channels-post-api/#picture-message
       */
      $limit = $messageType == 'text' ? 7000 : 768;
      switch ($messageType) {
        case'text':
          $markup = $form_state->getValue(['message', 'value']);
          $client->withBody('type', 'text');
          break;

        case'photo':
          $markup = $form_state->getValue(['caption', 'value']);
          $photo = '';

          /** @var File $file */
          $file = $this->entityTypeManager->getStorage('file')
            ->load($form_state->getValue(['file', 0]));
          if (!empty($file)) {
            $uri = $file->getFileUri();
            $url = $this->fileUrlGenerator->generateAbsoluteString($uri);
            $photo = Url::fromUri($url)->toString();
          }

          $client
            ->withBody('type', 'picture')
            ->withBody('media', $photo);
          break;
      }

      $message = $this->prepareMessageText($markup, $limit);
      $client->withBody('text', $message);
      $this->responseValidate($client, $form_state, $message);

    }
  }

  /**
   * @param string $markup
   * @param int $limit
   *
   * @return false|string
   * @throws \Exception
   */
  public function prepareMessageText(string $markup, int $limit = 768) {
    $html = Html::load($markup);
    $message = [
      '#type' => 'processed_text',
      '#text' => $html->saveHTML(),
      '#format' => 'viber_html',
    ];
    $message = $this->renderer->render($message);
    $message = strip_tags($message, $this::TAGS);
    return substr($message, 0, $limit);
  }

  /**
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function responseValidate(HttpClient $client, FormStateInterface $form_state, $message) {
    $result = $this->viberRequest->send($client);

    if (!empty($result)) {
      $logger = \Drupal::logger('viber_channel');
      switch ($result['status']) {
        case 0:
          $this->sendLog($form_state, [$message]);
          $this->messenger()
            ->addMessage($this->t('Message sent successfully.'), 'status', FALSE);
          break;
        case 1:
          $logger->error($this->t('The webhook URL is not valid'));
          break;
        case 2:
          $logger->error($this->t('The authentication token is not valid'));
          break;
        case 3:
          $logger->error($this->t('There is an error in the request itself (missing comma, brackets, etc.)'));
          break;
        case 4:
          $logger->error($this->t('Some mandatory data is missing'));
          break;
        case 7:
          $logger->error($this->t('The account is blocked'));
          break;
        case 8:
          $logger->error($this->t('The account associated with the token is not a account.'));
          break;
        case 9:
          $logger->error($this->t('The account is suspended'));
          break;
        case 10:
          $logger->error($this->t('No webhook was set for the account'));
          break;
        case 12:
          $logger->error($this->t('Rate control breach'));
          break;
        default:
          $logger->error($this->t('	General error'));
      }
    }

    if ($result['status'] != 0) {
      $form_state->setErrorByName('sender', $this->t('Error: message send failed.'));
    }
  }

  /**
   * Write log about sent messages.
   *
   * @param FormStateInterface $form_state
   * @param array $data
   *
   * @return void
   */
  public function sendLog(FormStateInterface $form_state, array $data = []) {
    $form_state->set('sendRun', TRUE);
    if (!empty($form_state->get('sendNid'))) {
      \Drupal::service('viber_channel.query')->writeLog([
        'nid' => $form_state->get('sendNid'),
        'user' => \Drupal::currentUser()->getDisplayName(),
      ], $data);
    }
  }

  /**
   * @param $element
   *
   * @return array
   */
  public function textFormatHide($element): array {
    $element['format']['#prefix'] = Markup::create('<div style="display:none;">');
    $element['format']['#suffix'] = '</div>';
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }

}
