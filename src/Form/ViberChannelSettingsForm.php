<?php

namespace Drupal\viber_channel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\viber_channel\API\ViberApi;
use Drupal\viber_channel\Http\HttpCachedClient;
use Drupal\viber_channel\Http\HttpClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class ViberChannelSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const CONFIG_NAME = 'viber_channel.settings';

  /**
   *
   */
  const FORM_ID = 'viber_channel_settings';

  /**
   * @var
   */
  protected $viberService;

  /**
   * @var
   */
  protected $viberQuery;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->viberService = $container->get('viber_channel');
    $instance->viberQuery = $container->get('viber_channel.query');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::CONFIG_NAME)->get('config');
    $form['#prefix'] = '<div id="' . self::FORM_ID . '">';
    $form['#suffix'] = '</div>';

    if (!empty($config['auth_token'])) {
      $listOfChannelUsers = $this->viberQuery->getUsers();
      $form['info'] = [
        '#type' => 'details',
        '#title' => $this->t('Channel senders are available'),
        '#description' => $this->t('Details about list of senders'),
        '#open' => !(count($listOfChannelUsers) >= 1),
        'senders' => [
          '#type' => 'select',
          '#multiple' => TRUE,
          '#options' => array_column($listOfChannelUsers ?: [], 'user_name'),
          '#disabled' => TRUE,
        ],
        'user_fetch' => [
          '#type' => 'button',
          '#value' => $this->t('Get senders'),
          '#ajax' => [
            'callback' => [self::class, 'getAccountInfo'],
            'event' => 'click',
            'wrapper' => self::FORM_ID,
          ],
        ],
        'webhook' => [
          '#type' => 'submit',
          '#value' => $this->t('Set webhook'),
          '#id' => 'webhook',
        ],
      ];
    }

    $form['config'] = [
      '#tree' => TRUE,
      'bot' => ['#access' => FALSE],
      'auth_token' => [
        '#type' => 'textfield',
        '#title' => $this->t('Auth token'),
        '#required' => TRUE,
        '#default_value' => $config['auth_token'] ?? '',
        '#description' => $this->t('You can get it in the viber channel settings'),
      ],
      'node_types' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Types of materials'),
        '#options' => $this->viberService->getStorageTypeOptions(),
        '#default_value' => $config['node_types'] ?? [],
        '#required' => TRUE,
      ],
      'interval' => [
        '#type' => 'select',
        '#title' => $this->t('Run cron every'),
        '#description' => $this->t('Setting to delete uploaded images, because the viber channel refers to the url of the image address'),
        '#default_value' => $config['interval'] ?? NULL,
        '#required' => TRUE,
        '#options' => $this->getOptionsDateFormat(),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @return array
   */
  private function getOptionsDateFormat(): array {
    $options = [3600, 10800, 21600, 43200, 86400, 604800];
    \Drupal::moduleHandler()
      ->alter('viber_channel_options_time_cron', $options);
    $dateFormatter = \Drupal::service('date.formatter');
    return array_map([
      $dateFormatter,
      'formatInterval',
    ], array_combine($options, $options));
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable(self::CONFIG_NAME);
    $config->set('config', $form_state->getValue('config'));
    $config->save();
  }

  /**
   * Ajax callback for fetch account info.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public static function getAccountInfo(array &$form, FormStateInterface $form_state): array {
    $token = $form_state->getValue('config')['auth_token'];
    self::fetch($token);
    return $form;
  }

  /**
   * @param string $auth_token
   *
   * @return void
   */
  private static function fetch(string $auth_token) {
    try {
      $request = \Drupal::service('viber_channel.request');
      $query = \Drupal::service('viber_channel.query');
      $client = (new HttpCachedClient())->setMethod('POST')
        ->withBody('auth_token', $auth_token)
        ->setEndpointUri(ViberApi::ACCOUNT_INFO);
      $result = $request->send($client);

      if (!empty($result['members']) && $result['status'] == 0) {
        foreach ($result['members'] as $key => $user) {
          $query->createUser($user);
        }
      }
      $message = t('Fetched @count users', ['@count' => count($result['members'])], ['context' => 'viber_channel'])->__toString();
      \Drupal::logger('viber_channel')->notice($message);
    } catch (\Throwable $e) {
      \Drupal::logger('viber_channel')->error($e->getMessage());
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|void
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if (!empty($trigger) && $trigger['#id'] == 'webhook') {
      $auth_token = $form_state->getValue('config')['auth_token'];
      $url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
      $request = \Drupal::service('viber_channel.request');
      $client = (new HttpClient())->setMethod('POST')
        ->withBody('auth_token', $auth_token)
        ->withBody('url', $url)
        ->setEndpointUri(ViberApi::WEBHOOK);
      $result = $request->send($client);
      if ($result['status'] === 0) {
        $this->messenger()
          ->addMessage($this->t('Webhook installation completed successfully'));
      }
      else {
        $message = $this->t('The webhook installation failed with an error');
        $form_state->setError($form['info'], $message);
        return $form;
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

}
