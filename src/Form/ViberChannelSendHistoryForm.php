<?php

namespace Drupal\viber_channel\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class ViberChannelSendHistoryForm extends FormBase {

  /**
   * @var
   */
  protected $viberQuery;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return static
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->viberQuery = $container->get('viber_channel.query');
    return $instance;
  }

  /**
   * @return string
   */
  public function getFormId(): string {
    return 'viber_channel_send_history';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear history'),
      '#attributes' => ['class' => ['button', 'button--danger']],
    ];
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->viberQuery->clearLogs();
    $this->messenger()->addMessage($this->t('Logs are cleared'));
  }

}
