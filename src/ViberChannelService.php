<?php

namespace Drupal\viber_channel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\viber_channel\Model\ViberChannelServiceInterface;

/**
 *
 */
class ViberChannelService implements ViberChannelServiceInterface {

  /**
   * @var EntityTypeManagerInterface|null
   */
  private ?EntityTypeManagerInterface $entityTypeManager;

  /**
   * Construct new object of ViberChannelService.
   */
  public function __construct() {
    $container = \Drupal::getContainer();
    $this->entityTypeManager = $container->get('entity_type.manager');
  }


  /**
   * Create options from entity storage type
   * for using in select form element.
   *
   * @param string $storage_type
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStorageTypeOptions(string $storage_type = 'node_type'): array {
    $options = [];
    if($this->entityTypeManager->hasDefinition($storage_type)) {
      $storage = $this->entityTypeManager->getStorage($storage_type);
      foreach ($storage->loadMultiple() as $node_type) {
        $options[$node_type->id()] = $node_type->label();
      }
    }
    return $options;
  }
}
