<?php

namespace Drupal\viber_channel\Http;

/**
 * General interface for coordinating methods for client instance.
 *
 * @ingroup request
 */
interface HttpClientInterface {

  /**
   * Set response timeout.
   *
   * @param int $timeout
   *
   * @return $this
   */
  public function setTimeOut(int $timeout = 30): HttpClientInterface;

  /**
   * @return int
   */
  public function getTimeOut(): int;

  /**
   * @param string $uri
   *
   * @return $this
   */
  public function setEndpointUri(string $uri): HttpClientInterface;

  /**
   * @return string
   */
  public function getEndpointUri(): string;

  /**
   * @param string $method
   *
   * @return $this
   */
  public function setMethod(string $method): HttpClientInterface;

  /**
   * @return string
   */
  public function getMethod(): string;

  /**
   * Return an instance with the provided value replacing the specified header.
   *
   * While header names are case-insensitive, the casing of the header will
   * be preserved by this function, and returned from getHeaders().
   *
   * This method MUST be implemented in such a way as to retain the
   * immutability of the message, and MUST return an instance that has the
   * new and/or updated header and value.
   *
   * @param string $name Case-insensitive header field name.
   * @param string|string[] $value Header value(s).
   *
   * @return static
   */
  public function withHeader(string $name, $value): HttpClientInterface;

  /**
   * @param array $header
   *
   * @return $this
   */
  public function setHeader(array $header): HttpClientInterface;

  /**
   * Retrieves a message header value by the given case-insensitive name.
   *
   * This method returns an array of all the header values of the given
   * case-insensitive header name.
   *
   * If the header does not appear in the message, this method MUST return an
   * empty array.
   *
   * @param string $name Case-insensitive header field name.
   *
   * @return string An array of string values as provided for the given
   *    header. If the header does not appear in the message, this method MUST
   *    return an empty array.
   */
  public function getHeader(string $name): string;

  /**
   * Retrieves all message header values.
   *
   * The keys represent the header name as it will be sent over the wire, and
   * each value is an array of strings associated with the header.
   *
   * While header names are not case-sensitive, getHeaders() will preserve the
   * exact case in which headers were originally specified.
   *
   * @return string[][] Returns an associative array of the message's headers.
   *   Each key MUST be a header name, and each value MUST be an array of
   *   strings for that header.
   */
  public function getHeaders(): array;

  /**
   * Return query for current request.
   *
   * @return array
   */
  public function getQuery(): array;

  /**
   * Set query value with give name as key current body.
   *
   * @param $name
   * @param $value
   *
   * @return $this
   */
  public function withQuery($name, $value): HttpClientInterface;

  /**
   * Set body value with give name as key current body.
   *
   * @param string $name Case-insensitive header field name.
   * @param string|string[] $value Header value(s).
   *
   * @return static
   */
  public function withBody(string $name, $value): HttpClientInterface;

  /**
   * Set body with new data.
   *
   * @param $body
   *
   * @return $this
   */
  public function setBody($body): HttpClientInterface;

  /**
   * Gets the body of the message.
   *
   * @return string|false The array of body data.
   */
  public function getBody();

}
