<?php

namespace Drupal\viber_channel\Http;

/**
 * General interface for coordinating the caching receipted of content from a remote api.
 *
 * @ingroup request
 */
interface HttpCachedClientInterface extends HttpClientInterface {

  /**
   * Setting the cid (take cid as uri) cache.
   *
   * @param $data
   *
   * @return \Drupal\viber_channel\Http\HttpClientInterface
   */
  public function setCache($data): HttpClientInterface;

  /**
   * Return cache data.
   *
   * @return array|object
   */
  public function getCache();

  /**
   * Check cache data if exists.
   *
   * @return bool
   */
  public function hasCache(): bool;

  /**
   * Build cache cid.
   *
   * @return string
   */
  public function getCid(): string;

}
