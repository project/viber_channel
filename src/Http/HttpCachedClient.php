<?php

namespace Drupal\viber_channel\Http;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Cache;
use Drupal\viber_channel\Http\HttpClient;

/**
 * Coordinating the caching receipted of content from a remote api.
 *
 * @ingroup request
 */
class HttpCachedClient extends HttpClient implements HttpCachedClientInterface {

  /**
   * Construct of SalesDriveHttpRequest object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|null
   */
  private ?CacheBackendInterface $cache;

  /**
   * @var array|null
   */
  private ?array $cache_data;

  /**
   * Object constructor of HttpCachedClient.
   */
  public function __construct() {
    $this->cache = \Drupal::cache();
  }

  /**
   * {@inheritDoc}
   */
  public function getCache() {
    if ($this->hasCache()) {
      return $this->cache_data;
    }
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function setCache($data): HttpClientInterface {
    $cid = $this->getCid();
    $this->cache->set($cid, $data, Cache::PERMANENT, ['handy_cache_tags:http_cached:request']);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function hasCache(): bool {
    $cid = $this->getCid();
    $result = $this->cache->get($cid);
    if (!empty($result)) {
      $this->cache_data = $result->data;
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return string
   */
  public function getCid(): string {
    $uri = $this->getEndpointUri();
    $query = $this->getQuery();
    $string = $uri . '?' . \http_build_query($query);
    return \md5($string);
  }

}
