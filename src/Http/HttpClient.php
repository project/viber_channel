<?php

namespace Drupal\viber_channel\Http;

use Drupal\viber_channel\Http\HttpClientInterface as IHttpRequest;

/**
 * Class ContentRequest.
 *
 * Builds Request Entity to rest sources.
 *
 * @ingroup request
 */
class HttpClient implements IHttpRequest {

  /**
   * @var string|null The HTTP method for this request.
   */
  protected ?string $method;

  /**
   * @var string|null The API endpoint for this request.
   */
  protected ?string $endpoint;

  /**
   * @var array The headers to send with this request.
   */
  protected array $headers = [
    "Content-type" => 'application/json',
    "Accept" => "application/json",
    "Cache-Control" => 'no-cache',
    "Pragma" => 'no-cache',
  ];

  /**
   * @var array The body to send with this request.
   */
  protected array $body = [];

  /**
   * @var array The query parameters to send with this request.
   */
  protected array $query = [];

  /**
   * @var int
   */
  protected int $timeout = 30;

  /**
   * Set the endpoint for this request.
   *
   * @param string $endpoint
   *
   * @return IHttpRequest
   */
  public function setEndpoint(string $endpoint): IHttpRequest {
    $this->endpoint = $endpoint;

    return $this;
  }

  /**
   * The default headers used with every request.
   *
   * @return array
   */
  public function getDefaultHeaders(): array {
    return [
      'Content-Type' => 'application/json',
    ];
  }

  /**
   * Return the HTTP method for this request.
   *
   * @return string
   */
  public function getMethod(): string {
    return $this->method;
  }

  /**
   * Set the HTTP method for this request.
   *
   * @param string
   *
   * @return IHttpRequest
   */
  public function setMethod(string $method): IHttpRequest {
    $this->method = strtoupper($method);

    return $this;
  }

  /**
   * Merge query with new query given.
   *
   * @param array $query
   *
   * @return $this
   */
  public function mergeQuery(array $query): IHttpRequest {
    $this->query = array_merge($this->query, $query);
    return $this;
  }

  /**
   * @param $name
   * @param $value
   *
   * @return \Drupal\viber_channel\Http\HttpClientInterface
   */
  public function withQuery($name, $value): IHttpRequest {
    $this->query[$name] = $value;
    return $this;
  }

  /**
   * Return query for this request.
   *
   * @return array
   */
  public function getQuery(): array {
    return $this->query;
  }

  /**
   * @param array $query
   *
   * @return $this
   */
  public function setQuery(array $query): IHttpRequest {
    $this->query = array_merge($this->query, $query);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getTimeOut(): int {
    return $this->timeout;
  }

  /**
   * {@inheritDoc}
   */
  public function setTimeOut(int $timeout = 30): IHttpRequest {
    $this->timeout = $timeout;
    return $this;
  }

  /**
   * @param string $uri
   *
   * @return $this
   */
  public function setEndpointUri(string $uri): IHttpRequest {
    $this->endpoint = $uri;
    return $this;
  }

  /**
   * @return string
   */
  public function getEndpointUri(): string {
    return $this->endpoint;
  }

  /**
   * {@inheritDoc}
   */
  public function withHeader(string $name, $value): IHttpRequest {
    $this->headers[$name] = $value;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getHeader(string $name): string {
    if (!empty($this->headers[$name])) {
      return $this->headers[$name];
    }
    return '';
  }

  /**
   * {@inheritDoc}
   */
  public function setHeader(array $header): IHttpRequest {
    $this->headers = $header;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getHeaders(): array {
    return $this->headers;
  }

  /**
   * @param string $name
   * @param array|string $value
   *
   * @return $this
   */
  public function withBody(string $name, $value): self {
    $this->body[$name] = $value;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getBody() {
    if(empty($this->body)) {
      return FALSE;
    }
    return json_encode($this->body, JSON_FORCE_OBJECT);
  }

  /**
   * {@inheritDoc}
   */
  public function setBody($body): IHttpRequest {
    $this->body = $body;
    return $this;
  }

}
