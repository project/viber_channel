<?php

namespace Drupal\viber_channel\Query;

use Drupal\Core\Database\Connection;

/**
 *
 */
class ViberChannelQuery {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * @var string
   */
  protected string $userTable = 'viber_channel_users';

  /**
   * @var string
   */
  protected string $recordTable = 'viber_channel_send';

  /**
   * Construct new object of ViberChannelQuery class.
   *
   * @param \Drupal\Core\Database\Connection $database
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * @param array $data
   *
   * @return int|void|null
   */
  public function createUser(array $data) {
    $query = $this->database->merge($this->userTable)
      ->key('sid', $data['id'])
      ->fields([
        'sid' => $data['id'],
        'role' => $data['role'],
        'user_name' => $data['name'],
      ]);
    return $query->execute();
  }

  /**
   * @param string $sid
   *
   * @return mixed
   */
  public function getUser(string $sid) {
    $query = $this->database->select($this->userTable, 'user');
    $query->condition('user.sid', $sid, '=');
    $query->fields('user', []);
    return $query->execute()->fetch();
  }

  /**
   * @return mixed
   */
  public function getUsers() {
    $query = $this->database->select($this->userTable, 'user');
    $query->fields('user');
    return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * @throws \Exception
   */
  public function writeLog($data, $serialize) {
    return $this->database->insert($this->recordTable)->fields([
      'nid' => $data['nid'],
      'user' => $data['user'],
      'send_time' => time(),
      'data' => serialize($serialize),
    ])->execute();
  }

  /**
   * @return mixed
   */
  public function getLogs() {
    return $this->database->select($this->recordTable, 'log')
      ->extend('Drupal\\Core\\Database\\Query\\PagerSelectExtender')
      ->fields('log', [])
      ->execute()
      ->fetchAll();
  }

  /**
   * @return int
   */
  public function clearLogs(): int {
    return $this->database->delete($this->recordTable)->execute();
  }

}
