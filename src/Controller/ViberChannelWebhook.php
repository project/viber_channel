<?php

namespace Drupal\viber_channel\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class ViberChannelWebhook extends ControllerBase {

  /**
   * @return array
   */
  public function index(): array {
    return [];
  }
}
