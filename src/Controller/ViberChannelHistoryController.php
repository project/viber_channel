<?php

namespace Drupal\viber_channel\Controller;

use Drupal\viber_channel\Form\ViberChannelSendHistoryForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class ViberChannelHistoryController extends ControllerBase {

  /**
   * @var
   */
  protected $viberQuery;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return static
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->viberQuery = $container->get('viber_channel.query');
    return $instance;
  }

  /**
   * @return array
   */
  public function index(): array {
    $results = $this->viberQuery->getLogs(0, 2);
    $rows = [];
    if (!empty($results)) {
      foreach ($results as $result) {
        $rows[] = [
          $result->nid,
          $result->user,
          date('d.m.Y H:i', $result->send_time),
          [
            'data' => [
              '#type' => 'details',
              '#title' => $this->t('Message'),
              [
                '#type' => 'inline_template',
                '#template' => '{{ info.message|raw }}',
                '#context' => [
                  'info' => [
                    'message' => !empty($result->data) ? unserialize($result->data)[0] : NULL,
                  ],
                ],
              ],
            ],
            'style' => 'width: 45%',
          ],
        ];
      }
    }
    $build['sends'] = [
      '#type' => 'details',
      '#title' => $this->t('Submissions'),
      '#open' => TRUE,
      [
        '#theme' => 'table',
        '#header' => [
          'Id',
          $this->t('Username'),
          $this->t('Time'),
          '',
        ],
        '#rows' => $rows,
        '#empty' => $this->t('No results'),
      ],
    ];
    $build[] = [
      '#type' => 'pager',
    ];

    $build[] = $this->formBuilder()->getForm(ViberChannelSendHistoryForm::class);

    return $build;
  }

}
