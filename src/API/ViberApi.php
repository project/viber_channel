<?php

namespace Drupal\viber_channel\API;

/**
 * Get ready to automatically post your content to an unlimited audience!
 * Using the API described below you can configure
 * your system to post directly into your Viber Channel.Get started now.
 */
final class ViberApi {

  /**
   * The get_account_info request will fetch the account’s details as registered in Viber.
   * The account admin will be able to edit most of these details from his Viber client.
   *
   * @see https://developers.viber.com/docs/tools/channels-post-api/#get-account-info
   */
  const ACCOUNT_INFO = 'https://chatapi.viber.com/pa/get_account_info';

  /**
   * In order to post to your Channel you will need
   * to send a POST request with the below properties.
   *
   * @see https://developers.viber.com/docs/tools/channels-post-api/#destination-url
   */
  const POST = 'https://chatapi.viber.com/pa/post';

  /**
   * Url for set Webhook callback.
   *
   * @see https://developers.viber.com/docs/tools/channels-post-api/#set-webhook-response
   */
  const WEBHOOK = 'https://chatapi.viber.com/pa/set_webhook';

}
