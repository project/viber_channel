<?php

namespace Drupal\viber_channel\API;

use Drupal\viber_channel\Http\HttpClientInterface;

/**
 * General interface for coordinating the receipt of content from a remote api.
 *
 * @ingroup request
 */
interface IContentRequestManager {

  /**
   * Implements the sending of the request.
   *
   * @param HttpClientInterface $client
   *
   * @return array|false|mixed|object|void
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function send(HttpClientInterface $client);

}
