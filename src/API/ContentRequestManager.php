<?php

namespace Drupal\viber_channel\API;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\{LoggerChannel, LoggerChannelFactory};
use Drupal\viber_channel\Http\{HttpCachedClientInterface, HttpClientInterface};

/**
 * Implements receiving content from a remote api
 * through the provided instance of class which implements HttpClientInterface.
 *
 * @ingroup request
 */
class ContentRequestManager implements IContentRequestManager {

  /**
   * @var \GuzzleHttp\ClientInterface|null
   */
  private ?ClientInterface $httpClient;

  /**
   * @var \Drupal\Core\Logger\LoggerChannel|null
   */
  private ?LoggerChannel $logger;

  /**
   * Construct of BotContentService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   */
  public function __construct(
    ClientInterface      $http_client,
    LoggerChannelFactory $logger
  ) {
    $this->httpClient = $http_client;
    $this->logger = $logger->get('viber_channel');
  }

  /**
   * {@inheritDoc}
   */
  public function send(HttpClientInterface $client) {
    if ($client instanceof HttpCachedClientInterface) {
      if ($client->hasCache()) {
        return $client->getCache();
      }
      else {
        $result = $this->execute($client);
        $client->setCache($result);
        return $result;
      }
    }
    else {
      return $this->execute($client);
    }
  }

  /**
   * A method of calling a request and
   * processing its error with subsequent logging.
   *
   * @param \Drupal\viber_channel\Http\HttpClientInterface $client
   *
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function execute(HttpClientInterface $client): array {
    $result = [];
    $url = $client->getEndpointUri();

    try {
      // Send request
      $response = $this->httpClient->request($client->getMethod(), $url, [
        'headers' => $client->getHeaders(),
        'body' => $client->getBody() ?? '',
        'query' => $client->getQuery(),
        'timeout' => $client->getTimeOut(),
      ]);

    } catch (ServerException $e) {

      // Prepare error message.
      $time = (new \DateTime())->format('d F Y H:i');
      $message = $time . ':' . $e->getMessage() . PHP_EOL;
      $response = $e->getResponse()->getBody()->getContents();

      // Log error
      $this->logger->error(print_r($response, TRUE));
      $this->logger->error($message);
      $result = [
        'message' => $e->getMessage(),
      ];
    }

    // Fetch content if all is fine.
    if (!is_null($response) && empty($result) && $response->getStatusCode() == 200) {
      $response = $response->getBody()->getContents();
      $result = Json::decode($response);
    }

    return $result;
  }

}
