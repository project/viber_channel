# ContentRequestManager

Built to send flexible requests to an API endpoint.

### Using
```php
<?php

// Creating a request object.
$client = (new HttpCachedClientInterface())
  ->setBody([
    'text' => 'some',
    'target' => '_blank',
    'other' => 'body value'
  ])
  ->withBody('token', '72db79a9c0d3d0742c7b1')
  ->setEndpoint('/SOME_API_URL')
  ->setMethod('GET')
;

// Service call to manage requests.
$requestManager = \Drupal::service('viber_channel.request');

// Sending a request with the transfer of an object
// HttpCachedClientInterface for construction request-у.
$response = $requestManager->send($client);

```
