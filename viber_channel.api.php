<?php

/**
 * @file
 * Hooks related to Viber Channel module.
 */

/**
 * Changing the message that will be sent to the viber channel.
 *
 * @param array $messageInfo
 *   An array of data to send.
 *      - string: message
 *      - array: photo
 *          - fid
 *          - caption
 * @param string $type
 *   Message type:
 *    - text
 *    - photo
 */
function HOOK_viber_channel_message_alter(array $messageInfo = [], string $type = '') {}

/**
 * Changing the message that will be sent to the viber channel.
 * @param array $options
 *  Contains an array with the number of seconds after which
 *  files sent to Viber must be cleared
 */
function HOOK_viber_channel_options_time_cron_alter(array $options = [3600, 10800, 21600]) {}
